function etalonnage(C);
%calcul des trois lignes c1 c2 c3    
c1=C(1,1:3);
c2=C(2,1:3);
c3=C(3,1:3);
%facteur d'echelle lambda (note L ici)
L=1%MODIFIER
%parametres intrinseques uo vo alpha_u alpha_v
uo=1%MODIFIER
vo=1%MODIFIER
alpha_u=1%MODIFIER
alpha_v=1%MODIFIER

%parametres extrinseques 
r1= [2 0 0];%MODIFIER
r2= [0 2 0];%MODIFIER
%normalisation deux premieres lignes pour obtenir R
r1n=r1/norm(r1);
r2n=r2/norm(r2);
r3=[0 0 1];%MODIFIER;
R = [r1n;r2n;r3]
%translations tx ty tz
tx=0.1;%MODIFIER
ty=0.2;%MODIFIER
tz=0.3;%MODIFIER
t=[tx;ty;tz]

%affichage des reperes camera et objet
figure;
hold on;
%repere camera
O_cam = [0;0;0];
X_cam = [1;0;0];
Y_cam = [0;0;-1];
Z_cam = [0;1;0];
plot3([O_cam(1) X_cam(1)],[O_cam(2) X_cam(2)],[O_cam(3) X_cam(3)],'Color', [1, 0, 0]);
plot3([O_cam(1) Y_cam(1)],[O_cam(2) Y_cam(2)],[O_cam(3) Y_cam(3)],'Color', [0, 1, 0]);
plot3([O_cam(1) Z_cam(1)],[O_cam(2) Z_cam(2)],[O_cam(3) Z_cam(3)],'Color', [0, 0, 1]);
%repere objet (on le transforme par rapport au repere world de matlab)
Rwc = [1 0 0; 0 0 1; 0 -1 0];
O_obj = Rwc * t;
X_obj = Rwc * (t + R(:,1));
Y_obj = Rwc * (t + R(:,2));
Z_obj = Rwc * (t + R(:,3));
hold on;
plot3([O_obj(1) X_obj(1)],[O_obj(2) X_obj(2)],[O_obj(3) X_obj(3)],'Color', [1, 0, 0]);
plot3([O_obj(1) Y_obj(1)],[O_obj(2) Y_obj(2)],[O_obj(3) Y_obj(3)],'Color', [0, 1, 0]);
plot3([O_obj(1) Z_obj(1)],[O_obj(2) Z_obj(2)],[O_obj(3) Z_obj(3)],'Color', [0, 0, 1]);
grid on;
